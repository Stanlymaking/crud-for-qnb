USE [dbCRUDQNB]
GO
/****** Object:  Table [dbo].[Tbl_Vehicle]    Script Date: 04/15/2021 02:02:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Vehicle](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[category] [varchar](25) NOT NULL,
	[type] [varchar](25) NOT NULL,
	[enggine] [varchar](25) NOT NULL,
	[passengers] [int] NOT NULL,
 CONSTRAINT [PK_Tbl_Vehicle] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
